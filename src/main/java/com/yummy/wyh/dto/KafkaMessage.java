/**
 * 
 */
package com.yummy.wyh.dto;

import java.util.Date;

import lombok.Data;

/**
 * @author wangyahui1
 *
 */
@Data
public class KafkaMessage {

    private long id;
    
    private String msg;
    
    private Date time;
}
