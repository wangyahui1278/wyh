/**
 * 
 */
package com.yummy.wyh.dto;

import lombok.Data;

/**
 * @author wangyahui1
 *
 */
@Data
public class RedisDto {

    private String code;
    
    private String value;
}
