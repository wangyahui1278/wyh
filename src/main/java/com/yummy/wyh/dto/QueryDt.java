/**
 * 
 */
package com.yummy.wyh.dto;

import lombok.Data;

/**
 * @author wangyahui1
 *
 */
@Data
public class QueryDt {
    
    private String code;
    
    private Object value;
}
