/**
 * 
 */
package com.yummy.wyh.logback;




import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.CoreConstants;

/**
 * @author wangyahui1
 *
 */
public class FlowLogbackPatternLayout extends PatternLayout  {
    
    private static final Logger LOG = LoggerFactory.getLogger(FlowLogbackPatternLayout.class);
    
    @Override
    public String doLayout(ILoggingEvent event) {
        
        LOG.info("消息事件{}",event.getMessage());
        if (!isStarted()) {
            return CoreConstants.EMPTY_STRING;
        }
        return writeLoopOnConverters(event);
    }

}
