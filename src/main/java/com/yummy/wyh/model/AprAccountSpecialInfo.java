package com.yummy.wyh.model;

import java.util.Date;

public class AprAccountSpecialInfo {
    private Long id;

    private String accountAllotId;

    private String orgId;

    private String orgItemCode;

    private String orgItemVal;

    private String orgItemDesc;

    private Date createdDate;

    private String createdBy;

    private Date updatedDate;

    private String updatedBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountAllotId() {
        return accountAllotId;
    }

    public void setAccountAllotId(String accountAllotId) {
        this.accountAllotId = accountAllotId == null ? null : accountAllotId.trim();
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    public String getOrgItemCode() {
        return orgItemCode;
    }

    public void setOrgItemCode(String orgItemCode) {
        this.orgItemCode = orgItemCode == null ? null : orgItemCode.trim();
    }

    public String getOrgItemVal() {
        return orgItemVal;
    }

    public void setOrgItemVal(String orgItemVal) {
        this.orgItemVal = orgItemVal == null ? null : orgItemVal.trim();
    }

    public String getOrgItemDesc() {
        return orgItemDesc;
    }

    public void setOrgItemDesc(String orgItemDesc) {
        this.orgItemDesc = orgItemDesc == null ? null : orgItemDesc.trim();
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy == null ? null : updatedBy.trim();
    }
}