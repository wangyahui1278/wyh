package com.yummy.wyh.sao;

import com.yummy.wyh.intercepter.FeignInterceptorConfig;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * @program: wyh
 * @ClassName TestSao
 * @description:
 * @author: wangyahui
 * @create: 2021-05-21 20:45
 * @Version 1.0
 **/

//configuration = FeignInterceptorConfig.class 这个是局部feign调用使用该拦截器
@FeignClient(name="test",configuration = FeignInterceptorConfig.class)
public class TestSao {

    /**
     * 发起调用
     */
    @PostMapping("aaa/bbb")
    public void test(@RequestBody Map<String, Object> m) {

    }
}
