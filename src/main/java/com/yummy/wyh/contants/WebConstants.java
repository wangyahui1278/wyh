package com.yummy.wyh.contants;

/**
 * @program: wyh
 * @ClassName WebConstants
 * @description:
 * @author: wangyahui
 * @create: 2021-05-21 22:16
 * @Version 1.0
 **/
public interface WebConstants {

     String HEADER_FOR_USER_ID = "x_user_id";

     String HEADER_FOR_USER_NAME = "x_user_name";

     String HEADER_FOR_TOKEN ="x_token";

     String HEADER_FOR_LOG_LEVEL = "x_log_level";

     String HEADER_FOR_SYSTEM = "x_sysytem";

}
