/**
 * 
 */
package com.yummy.wyh.service;

import java.util.Date;
import java.util.Optional;

import org.apache.kafka.clients.consumer.ConsumerRecord;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.Acknowledgment;
//import org.springframework.kafka.annotation.KafkaListener;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.yummy.wyh.dto.KafkaMessage;

import lombok.extern.slf4j.Slf4j;

/**
 * @author wangyahui1
 * 
 */
@Service
@Slf4j
public class KafkaOperateService {
    
    @Autowired
    private KafkaTemplate kafkaTemplate;
    
    private static final String MY_TOPIC = "wyhtest";
    
    private static final String CO_TOPIC = "test";
    
    public void produce(){
        KafkaMessage message = new KafkaMessage();
        message.setId(12L);
        message.setMsg("hello jack");
        message.setTime(new Date());
        
        kafkaTemplate.send(MY_TOPIC, JSONObject.toJSONString(message));
    }
    
    
   // @KafkaListener(topics = {CO_TOPIC})
    public void consume(String message){
        log.info("receive msg "+ message);
    }
    
    //@KafkaListener(topics = {MY_TOPIC})
    public void topic_test(ConsumerRecord<?, ?> record, Acknowledgment ack, String topic) {

        Optional message = Optional.ofNullable(record.value());
        if (message.isPresent()) {
            Object msg = message.get();
            log.info("topic_test 消费了： Topic:" + topic + ",Message:" + msg);
            ack.acknowledge();
        }
    }

}
