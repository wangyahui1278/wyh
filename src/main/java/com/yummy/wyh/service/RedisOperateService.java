/**
 * 
 */
package com.yummy.wyh.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.DefaultTypedTuple;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations.TypedTuple;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.yummy.wyh.utils.RedisUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * @author wangyahui1
 *
 */
@Slf4j
@Service
public class RedisOperateService {
 
 @Autowired
 private RedisUtil redisUtil;
 
 @Autowired
 private StringRedisTemplate redisTemplate;

 
 public void rediszTest() {
     //测试zset
     Set<TypedTuple> set1 = new HashSet<TypedTuple>();
     Set<TypedTuple> set2 = new HashSet<TypedTuple>();
     int j = 9;
     for (int i = 1; i <= 9; i++) {
         //计算分数和值
         Double score1 = Double.valueOf(i);
         String value1 = "x" + i;
         Double score2 = Double.valueOf(j);
         String value2 = j % 2 == 1 ? "Y" + j : "X" + j;
         //使用Spring提供的默认TypedTuple----DefaultTypedTuple
         TypedTuple<String> typedTuple1 = new DefaultTypedTuple<String>(value1, score1);
         set1.add(typedTuple1);
         TypedTuple<String> typedTuple2 = new DefaultTypedTuple<String>(value2, score2);
         set2.add(typedTuple2);
     }
     redisUtil.set("zset1", set1);
     redisUtil.set("zset2", set2);
     Set set = redisUtil.rangeWithScores("zset1", 0, -1);
     System.out.println(set.size());
     log.info(JSONObject.toJSONString(set));
     
 }
     
}
