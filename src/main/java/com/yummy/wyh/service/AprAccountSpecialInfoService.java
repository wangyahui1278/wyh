package com.yummy.wyh.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.yummy.wyh.dto.QueryDt;
import com.yummy.wyh.mapper.AprAccountSpecialInfoMapper;
import com.yummy.wyh.model.AprAccountSpecialInfo;

import lombok.extern.slf4j.Slf4j;

/**
 * @author wangyahui1
 *
 */
@Slf4j
@Service
public class AprAccountSpecialInfoService {
    
    @Autowired
    private AprAccountSpecialInfoMapper aprAccountSpecialInfoMapper;
    
//    @NonRepeatable(key = "getAprAccountSpecialInfoById", params = {"#{id}"},keepTime = 15000)
    public AprAccountSpecialInfo getAprAccountSpecialInfoById(String id){
        log.info("开始请求了");
        List <String> list = new ArrayList<String>();
        list.add("id");
        list.add("org_id");
        list.add("account_allot_id");
        QueryDt queryDt = new QueryDt();
        queryDt.setCode("id");
        queryDt.setValue(11L);
        List<QueryDt> aa = new ArrayList<QueryDt>();
        aa.add(queryDt);
        QueryDt queryDt1 = new QueryDt();
        queryDt1.setCode("account_allot_id");
        queryDt1.setValue("RL20190416000215");
        aa.add(queryDt1);
        List<Map<String, Object>> policyScoreLevelList = aprAccountSpecialInfoMapper.selectByPrimaryKey(list,aa);
        log.info(JSONObject.toJSONString(policyScoreLevelList));
        AprAccountSpecialInfo n = new AprAccountSpecialInfo();
        return n;
    }
   
}
