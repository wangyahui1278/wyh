/**
 * 
 */
package com.yummy.wyh.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.google.gson.JsonObject;
import com.yummy.wyh.model.AprAccountSpecialInfo;
import com.yummy.wyh.service.AprAccountSpecialInfoService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author wangyahui1
 *
 */
@Slf4j
@Component
public class TestJob implements SimpleJob{
    
//    @Autowired
//    private AprAccountSpecialInfoService aprAccountSpecialInfoService;
    
    /* (non-Javadoc)
     * @see com.dangdang.ddframe.job.api.simple.SimpleJob#execute(com.dangdang.ddframe.job.api.ShardingContext)
     */
    @Override
    public void execute(ShardingContext shardingContext) {
        // TODO Auto-generated method stub
        long start = System.currentTimeMillis();
        log.info("自动获取信息job开始------------");
//        AprAccountSpecialInfo aprAccountSpecialInfo = aprAccountSpecialInfoService.getAprAccountSpecialInfoById(1);
//        log.info("自动获取信息{}",JSON.toJSONString(aprAccountSpecialInfo));
        log.info("自动获取信息job结束，耗时[{}ms]------------", System.currentTimeMillis() - start);
    }
}
