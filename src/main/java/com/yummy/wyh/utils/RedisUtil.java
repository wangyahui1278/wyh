/**
 * 
 */
package com.yummy.wyh.utils;

import java.util.Date;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisZSetCommands;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Component;

/**
 * @author wangyahui1
 *
 */
@Component
public class RedisUtil {
    
    @Autowired
    private RedisTemplate redisTemplate;

    public <T> Boolean setIfAbsent(String key, T value) {
        ValueOperations<String, T> valueOperations = redisTemplate.opsForValue();
        return valueOperations.setIfAbsent(key, value);
    }

    public Boolean expire(final String key, final long timeout, final TimeUnit unit) {
        return redisTemplate.expire(key, timeout, unit);
    }

    public <T> Boolean expireAt(T key, final Date date) {
        return redisTemplate.expireAt(key, date);
    }

    public void delete(final String key) {
        redisTemplate.delete(key);
    }

    public <T> void set(String redisKey, T obj, double score) {
        ZSetOperations<String, T> zSetOperations = this.redisTemplate.opsForZSet();
        //zset内部是按分数来排序的
        zSetOperations.add(redisKey, obj, score);
    }
    
    public <T>void set(String key,Set tuples){
        ZSetOperations<String, T> zSetOperations = this.redisTemplate.opsForZSet();
        //zset内部是按分数来排序的
        zSetOperations.add(key, tuples);
    }


    public <T> Double score(String redisKey, T obj) {
        ZSetOperations<String, T> zSetOperations = this.redisTemplate.opsForZSet();
        return zSetOperations.score(redisKey, obj);
    }

    /**
     * 移除key相关的成员
     *
     * @param redisKey
     * @param start
     * @param end
     * @param <T>
     * @return
     */
    public <T> Long removeRange(String redisKey, long start, long end) {
        ZSetOperations<String, T> zSetOperations = this.redisTemplate.opsForZSet();
        return zSetOperations.removeRange(redisKey, start, end);
    }

    public <T> Long remove(String redisKey, Object... values) {
        ZSetOperations<String, T> zSetOperations = this.redisTemplate.opsForZSet();
        return zSetOperations.remove(redisKey, values);
    }

    /**
     * 移除的是成员value
     *
     * @param redisKey
     * @param min
     * @param max
     * @param <T>
     * @return
     */
    public <T> Long removeRangeByScore(String redisKey, double min, double max) {
        ZSetOperations<String, T> zSetOperations = this.redisTemplate.opsForZSet();
        return zSetOperations.removeRangeByScore(redisKey, min, max);
    }

    public <T> Long zCard(String redisKey) {
        ZSetOperations<String, T> zSetOperations = this.redisTemplate.opsForZSet();
        return zSetOperations.zCard(redisKey);
    }

    public <T> Set range(String redisKey, long start, long end) {
        ZSetOperations<String, T> zSetOperations = this.redisTemplate.opsForZSet();
        return zSetOperations.range(redisKey, start, end);
    }

    /*
     * 按照【分数】排序对指定区间取值和分数
     */
    public <T> Set rangeByScoreWithScores(String score, double min, double max) {
        ZSetOperations<String, T> zSetOperations = this.redisTemplate.opsForZSet();
        return zSetOperations.rangeByScoreWithScores(score, min, max);
    }

    /**
     * 获取下标
     *
     * @param redisKey
     * @param v
     * @param <T>
     * @return
     */
    public <T> Long rank(String redisKey, Object v) {
        ZSetOperations<String, T> zSetOperations = this.redisTemplate.opsForZSet();
        return zSetOperations.rank(redisKey, v);
    }

    /**
     * 获取区间的个数
     *
     * @param redisKey
     * @param min
     * @param max
     * @param <T>
     * @return
     */
    public <T> Long count(String redisKey, double min, double max) {
        ZSetOperations<String, T> zSetOperations = this.redisTemplate.opsForZSet();
        return zSetOperations.count(redisKey, min, max);
    }

    public <T> Set rangeByLex(String redisKey, RedisZSetCommands.Range range) {
        ZSetOperations<String, T> zSetOperations = this.redisTemplate.opsForZSet();
        return zSetOperations.rangeByLex(redisKey, range);
    }

    public <T> Set rangeByScore(String key, double min, double max) {
        ZSetOperations<String, T> zSetOperations = this.redisTemplate.opsForZSet();
        return zSetOperations.rangeByScore(key, min, max);
    }

    public <T> Set rangeByScore(String key, double min, double max, long offset, long count) {
        ZSetOperations<String, T> zSetOperations = this.redisTemplate.opsForZSet();
        return zSetOperations.rangeByScore(key, min, max, offset, count);
    }
    
    public <T> Set rangeWithScores(String key,long start,long end) {
        return redisTemplate.opsForZSet().rangeWithScores(key, start, end);
    }
}
