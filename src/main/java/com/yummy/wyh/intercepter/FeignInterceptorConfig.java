package com.yummy.wyh.intercepter;

import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;

/**
 * @program: wyh
 * @ClassName FeignInterceptorConfig
 * @description:
 * @author: wangyahui
 * @create: 2021-05-21 22:43
 * @Version 1.0
 **/
public class FeignInterceptorConfig {
    @Bean
    public RequestInterceptor getFeignInterceptor(){
        return new FeignInterceptor();
    }

}
