package com.yummy.wyh.intercepter;

import com.yummy.wyh.contants.WebConstants;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Configuration;

/**
 * @program: wyh
 * @ClassName FeignInterceptor
 * @description: feign拦截器
 * @author: wangyahui
 * @create: 2021-05-21 20:40
 * @Version 1.0
 **/

//如果使用@Configuration作用在自定义拦截器上，则全局生效，任何通过feign发出去的请求，都会通过这个类设置head。
// 如只想对某些服务设置拦截器，则将该注解去除，然后看@See FeignInterceptorConfig TestSao
@Configuration
public class FeignInterceptor implements RequestInterceptor {

    private static final String userId = "root";

    private static final String userName = "wyh";


    @Override
    public void apply(RequestTemplate template) {
        template.header(WebConstants.HEADER_FOR_USER_ID,userId)
        .header(WebConstants.HEADER_FOR_USER_NAME,userName);
    }
}
