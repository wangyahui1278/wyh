/**
 * 
 */
package com.yummy.wyh.annatation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 不可重复调用方法的声明注解，此注解可能只在特定的上下文中支持，如依赖于spring的starter模块。
 * @author wangyahui1
 *
 */

@Repeatable(NonRepeatables.class)
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface NonRepeatable {
    
    /**
     * key,如不传，则自动设置成：包路径.类名.方法名
     */
    String key() default "";
    
    /**
          * 参数。例如：#{applicationId}, 或者#{dto.applicationId}
     */
    String[] params();
    
    /**
         * 获得锁的持有时间,超过此时间自动释放（即重新调用），以防死锁。单位：毫秒。默认5分钟
     */
    long keepTime() default 30000l;
    
}
