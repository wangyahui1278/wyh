/**
 * 
 */
package com.yummy.wyh.annatation;

import java.lang.reflect.Method;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.alibaba.druid.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * {@link NonRepeatable}注解的处理器
 * @author wangyahui1
 *
 */

@Slf4j
@Component
@Aspect
@Order(-100)
public class NonRepeatableAspect {
    
    
    /**
     * 类型与方法之间的分隔符
     */
    private static final String TYPE_METHOD_SEPARATOR = ".";
    /**
     * key分隔符
     */
    private static final String KEY_SEPARATOR = "_";
    
    @Autowired
    private RedisLocker locker;
    
    //创建Pointcut表示式 表示所有的NonRepeatable注解的请求
    @Pointcut("execution(@com.yummy.wyh.annatation.NonRepeatable public * *(..))")
    private void nonRepeatableMethodAspect() {
    }// 请求method前打印内容
    
    /**
        * 拦截处理带有{@link NonRepeatable} 注解的方法
     *  <p>
     * 1、根据注解的参数值，获取锁；
     * <p>
     * 2、在获取锁后执行业务代码；
     * <p>
     * 3、在业务代码完成后释放锁。
     * 
     * @param proceedingJoinPoint aop连接点
     * @return 业务代码返回值
     * @throws Throwable 可能抛出异常
     */
    
    @Around(value = "nonRepeatableMethodAspect()")
    public Object processWithLock(ProceedingJoinPoint proceedingJoinPoint) throws Throwable{
        Signature signature = proceedingJoinPoint.getSignature();
        if (!(signature instanceof MethodSignature)) {
            throw new IllegalStateException("不支持的拦截点，签名类型为: " + signature.getClass().getName());
        }
        MethodSignature methodSignature = (MethodSignature) signature;
        // 从连接点中获取目标对象、目标方法和方法参数信息
        Method method = methodSignature.getMethod();
        NonRepeatable nonRepeatableCall = method.getAnnotation(NonRepeatable.class);
        String key = nonRepeatableCall.key();
        if (StringUtils.isEmpty(key)) {
            key = proceedingJoinPoint.getTarget().getClass().getName() + TYPE_METHOD_SEPARATOR
                    + method.getName();
        }
        String[] params = nonRepeatableCall.params();
        if (params.length > 0) {
            for (String param : params) {
                key += KEY_SEPARATOR;
                key += AnnotationResolver.newInstance().resolver(proceedingJoinPoint, param);
            }
        }else {
            log.info("param为空 调用NonRepeatable错误 key为：" + key);
            throw new IllegalStateException("拦截点参数错误，签名类型为: " + signature.getClass().getName());
        }
        log.info("key=={}" , key);
        boolean lockSuccess = false;
        try {
            lockSuccess = locker.tryLock(key, nonRepeatableCall.keepTime());
            return proceedingJoinPoint.proceed();
        } finally {
            if (lockSuccess) {
                locker.unlock(key);
            }
        }
    }
}
