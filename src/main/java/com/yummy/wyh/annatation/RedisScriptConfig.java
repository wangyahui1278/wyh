package com.yummy.wyh.annatation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;

/**
 * lua脚本
 *
 * @author wangyahui1
 *
 */
@Configuration
public class RedisScriptConfig {

    /**
     * 加锁
     */
    @Bean(name = "lockScript")
    public RedisScript<Boolean> lockScript() {
        StringBuilder lockScript = new StringBuilder();
        lockScript.append("if (redis.call('setnx', KEYS[1], ARGV[1]) == 1) then ");
        lockScript.append("  redis.call('pexpire', KEYS[1], ARGV[2]); ");
        lockScript.append("  return 1; ");
        lockScript.append("else ");
        lockScript.append("  return 0; ");
        lockScript.append("end; ");

        DefaultRedisScript<Boolean> redisScript = new DefaultRedisScript<>();
        redisScript.setScriptText(lockScript.toString());
        redisScript.setResultType(Boolean.class);

        return redisScript;
    }

    /**
     * 解锁
     */
    @Bean(name = "unlockScript")
    public RedisScript<String> unlockScript() {
        StringBuilder unlockScript = new StringBuilder();
        unlockScript.append("if (redis.call('exists', KEYS[1]) == 1) then ");
        unlockScript.append("  if (redis.call('get', KEYS[1]) == ARGV[1]) then ");
        unlockScript.append("    redis.call('del', KEYS[1]); ");
        unlockScript.append("    return '1'; ");
        unlockScript.append("  else ");
        unlockScript.append("    return '2'; ");
        unlockScript.append("  end; ");
        unlockScript.append("else ");
        unlockScript.append("  return '0'; ");
        unlockScript.append("end; ");

        DefaultRedisScript<String> redisScript = new DefaultRedisScript<>();
        redisScript.setScriptText(unlockScript.toString());
        redisScript.setResultType(String.class);

        return redisScript;
    }
}
