package com.yummy.wyh.annatation;

import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.stereotype.Component;

/**
 * redis分布式锁
 *
 * @author wangyahui
 * 
 */
@Slf4j
@Component
public class RedisLocker {

    /**
     * redis的key值前缀
     */
    private static String KEY_PRE = "WYH:STRING:LOCKER:";
    /**
     * 默认持有时间10分钟
     */
    private static long DEFAULT_KEEP_TIME = 600000L;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    @Qualifier("lockScript")
    private RedisScript<Boolean> lockScript;
    @Autowired
    @Qualifier("unlockScript")
    private RedisScript<String> unlockScript;

    /**
     * 竞争当前锁
     * <p>
     * 持有时间默认为10分钟
     *
     * @param key key值
     * @return 竞争锁结果（false 失败，true 成功）
     */
    public boolean tryLock(String key) {
        return this.tryLock(key, DEFAULT_KEEP_TIME);
    }

    /**
     * 加锁
     *
     * @param key key值
     * @param keepTime 持有时间（单位：毫秒）
     * @return 竞争锁结果（false 失败，true 成功）
     */
    public boolean tryLock(String key, long keepTime) {
        log.info("【加锁】开始，key={}, keepTime={}", key, keepTime);
        String redisKey = KEY_PRE + key;
        String threadId = String.valueOf(Thread.currentThread().getId());
        List<String> keys = new ArrayList<>();
        keys.add(redisKey);
        Boolean result = redisTemplate.execute(lockScript, keys,
                threadId, String.valueOf(keepTime));
        log.info("【加锁】结束，key={}, keepTime={}, result={}", key, keepTime, result);
        return result == null ? false : result;
    }

    /**
     * 解锁，如果没有获取到锁，执行释放锁，什么都没发生
     *
     * @param key key值
     */
    public void unlock(String key) {
        log.info("【解锁】开始，key={}", key);
        String redisKey = KEY_PRE + key;
        String threadId = String.valueOf(Thread.currentThread().getId());
        List<String> keys = new ArrayList<>();
        keys.add(redisKey);
        String result = redisTemplate.execute(unlockScript, keys, threadId);
        if ("0".equals(result)) {
            log.warn("【解锁】失败，锁不存在，key={}", key);
        } else if ("2".equals(result)) {
            log.warn("【解锁】失败，锁不属于当前线程，key={}", key);
        } else {
            log.info("【解锁】结束，key={}", key);
        }
    }
}

