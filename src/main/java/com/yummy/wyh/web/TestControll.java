/**
 * 
 */
package com.yummy.wyh.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.yummy.wyh.model.AprAccountSpecialInfo;
import com.yummy.wyh.service.AprAccountSpecialInfoService;
import com.yummy.wyh.service.KafkaOperateService;
import com.yummy.wyh.service.RedisOperateService;

/**
 * @author wangyahui1
 *
 */
@RestController
public class TestControll {
    
    @Autowired
    private AprAccountSpecialInfoService aprAccountSpecialInfoService;
    
    @Autowired
    private RedisOperateService redisOperateService;
    
    @Autowired
    private KafkaOperateService kafkaOperateService;
    
    @GetMapping(value = "/queryPartnerTable")
    @ResponseBody
    public AprAccountSpecialInfo queryPartnerTable() {
            return aprAccountSpecialInfoService.getAprAccountSpecialInfoById("RL20190416000215");
        
    }
    @GetMapping(value = "/testredis")
    @ResponseBody
    public void testRedis() {
        redisOperateService.rediszTest();
    }
    
    
//    @GetMapping(value = "/testkafka")
//    @ResponseBody
//    public void testkafka() {
//        kafkaOperateService.produce();
//    }
    
}
