package com.yummy.wyh.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.yummy.wyh.dto.QueryDt;
import com.yummy.wyh.model.AprAccountSpecialInfo;

@Mapper
public interface AprAccountSpecialInfoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(AprAccountSpecialInfo record);

    int insertSelective(AprAccountSpecialInfo record);

    List<Map<String, Object>> selectByPrimaryKey(@Param("list")List<String> list,@Param("aa") List<QueryDt> aa);

    int updateByPrimaryKeySelective(AprAccountSpecialInfo record);

    int updateByPrimaryKey(AprAccountSpecialInfo record);
}