/**
 * 
 */
package com.yummy.wyh;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author wangyahui1
 *
 */
@Configuration
@ComponentScan(basePackages = { "com.yummy.wyh" })
public class TestConfig {

}
