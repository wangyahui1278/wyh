/**
 * 
 */
package com.yummy.wyh;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author wangyahui1
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest()
@ContextConfiguration(classes = { TestConfig.class })
public class CircularDependencyTest {
    @Test
    public void givenCircularDependency_whenConstructorInjection_thenItFails() {

        // Empty test; we just want the context to load

    }
}
