/**
 * 
 */
package com.yummy.wyh;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author wangyahui1
 *
 */
@Component
public class CircularDependencyA {
    @Autowired
    private CircularDependencyB circB;
}
